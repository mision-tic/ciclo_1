import numpy as np

from numpy import random as r


print(r.choice(['Andres','Juan','Pedro', 'Mateo'], size= r.choice([1,2],p=[0.1,0.9]) , p=[0.5,0.2,0.2,0.1], replace=False))



a = np.array([34,25,7])
print(type(a))
print(a.shape) # .shape nos retorna el tamaño o forma de la matriz 
print(a[0], a[1], a[2]) # nos retorna el elemento en la posicion especificada
a[0] = 5 #cambiar el elemento de la posiscion 0 por un 5
print(a)



b = np.array([[1,2,3],[4,5,6]]) #Crear una matriz de rango 2

print(b.shape) # 2 filas, cada una de 3 columnas

print(b[0,0], b[0,1], b[0,2]) # En la posicion 0, elemento 1,...

matriz = np.zeros((3,3)) #Crear una matriz de ceros de 3 filas por 3 columnas
print(matriz)


b_one = np.ones((2,3)) #Crear una matriz de unos de 2 filas por 3 columnas
print(b_one)


c_full = np.full((5,5),8) #Crear una matriz de el valor que se le indique (8) de tamaño (5,5)
print(c_full)

d = np.eye(4) #crea la matriz identidad del tamaño que se le indique (4)
print(d)

e = np.random.random((2,2)) #Crea una matriz de valores aleatorios del tamaño que se le indica (2,2)
print(e)

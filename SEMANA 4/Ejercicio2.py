#ENVOLTURA DE FUNCIONES EN PYTHON
'''
def suma(val1=0, val2=0): #se pone igual a cero para inicializar
    return val1+val2

#print(suma(1,1))


def operacion(funcion,val1=0,val2=0):
    return funcion(val1,val2)
# asignar una función a una variable
funcion_suma = suma
#enviar como parámetro una función
resultado = operacion(funcion_suma,3,4)
print(resultado)
'''
'''
def crear_funcion(operador):
    if operador == '+':
        def suma(var1=0,var2=0):
            return var1 + var2
        return suma
    elif operador == '-':
        def resta(var1=0,var2=0):
            return var1 - var2
        return resta    

def operacion(funcion,var1=0,var2=0):
    return funcion(var1,var2)

funcion_suma = crear_funcion('+')
resultado = operacion(funcion_suma,7,9)
print(resultado)

funcion_resta = crear_funcion('-')
resultado = operacion(funcion_resta,19,8)
print(resultado)
'''
'''
def suma(var1=0,var2=0):
    return var1 + var2
'''
'''
sumar = lambda val1 = 0 , val2 = 0: val1 + val2
operacion = lambda operacion, var1 = 0, var2 = 0 : operacion(var1,var2)

resultado = operacion(sumar,7,8)
print(resultado)
'''
'''
sin_parametros = lambda : True 
print(sin_parametros()==True)

con_valores = lambda val=0, val_1=0, val_2=0 : val+val_1+val_2
resultado = con_valores(3,6,7)
print(resultado)

con_asterisco = lambda * arg : arg[0]
resultado = con_asterisco('yuo')
print(resultado)
'''
'''
con_asterisco = lambda * arg : arg[0]

con_doble_asterisco = lambda**args : args[0]
resultado = con_doble_asterisco()
print(resultado)
'''

resultado = lambda ** kwargs: sum(kwargs.values())(a=1,b=2,c=5)

print((lambda ** kwargs: sum(kwargs.values()))(a=1,b=2,c=5))

import numpy as np


# Crear una nueva matriz de la cual seleccionaremos elementos

a = np.array([[1,2,3],[4,5,6],[7,8,9],[10,11,12]])
#print(a)

# Crear una serie de índices

b = np.array([0,2,0,1]) 
#print(b)

# Selecciona un elemento de cada fila de a usando los índices de b

print(a[np.arange(4),b]) #[1 6 7 11]
#Arange:  genera un conjunto
#  de números entre un valor de inicio y uno final, pudiendo especificar un incremento entre los valores
#Si solo añadimos un valor como argumento, la función considera todos los valores desde el cero hasta dicho valor
a[np.arange(4), b] += 10

print(a)
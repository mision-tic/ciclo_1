#Indexación de matrices de enteros:

import numpy as np

a = np.array(
    [
        [1,2],
        [3,4],
        [5,6]
    ]
)

#print(a.shape)
#print(a[[0,1,2],[0,1,0]]) #de la fila 0, el elemento 0, de la fila 1, el elemento 1, y de la fila 2, el elemento 0

#print(np.array([a[0,0], a[1,1], a[2,0]])) #[1 4 5]
print(a[[0,0],[1,1]]) #[2 2] ; [de la fila 0 el elemento 1,de la fila 0 el elemento 1]

print(np.array([a[0, 1], a[0, 1]])) # [2 2]
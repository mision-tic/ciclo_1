
lectura = {
            '201501001' :{
                           'toma_lectura': [
                                             {
                                               'lec_anterior': 12,
                                               'lec_actual': 60,
                                             }
                                           ],
                          'estrato': 1,
                          'estado': 'activo'
                         },
            '201501002' :{
                           'toma_lectura': [
                                            {
                                             'lec_anterior': 2,
                                             'lec_actual': 6,
                                            }
                                            ],
                           'estrato': 2,
                           'estado': 'activo'
                         },
            '201501003' :{
                            'toma_lectura': [
                                             {
                                              'lec_anterior': 23,
                                              'lec_actual': 43,
                                             }
                                            ],
                            'estrato': 3,
                            'estado': 'activo'
                         },
            '201501004' :{
                            'toma_lectura': [
                                             {
                                              'lec_anterior': 90,
                                              'lec_actual': 120,
                                             }
                                            ],
                            'estrato': 1,
                            'estado': 'activo'
                         },
            '201501005' :{
                            'toma_lectura': [
                                             {
                                              'lec_anterior': 1,
                                              'lec_actual': 9,
                                             }
                                            ],
                            'estrato': 1,
                            'estado': 'inactivo'
                         },
            '201564006' :{
                            'toma_lectura': [
                                             {
                                              'lec_anterior': 10,
                                              'lec_actual': 20,
                                             }
                                            ],
                            'estrato': 6,
                            'estado': 'activo'
                         }
    }


tarifa = {
           'cargo_basico': 9850,
           'consumo': 800.27
         }


def inforServicio(lectura : dict, tarifa : dict)-> tuple:
    lecturas_anteriores_lista, lecturas_actuales_lista, parte_1 ,total_subsidio_lista,usuarios = [],[],[],[],[]
    total_liquidado_lista,  inf_estratos_lista = [], []
    a=list(tarifa.values())
    cargo_basico,consumo = a[0],a[1]
    for elementos,información in lectura.items():
        if información['estado'] == 'inactivo':
            continue  
        lectura_anterior = información['toma_lectura'][0]['lec_anterior']
        lecturas_anteriores_lista.append(lectura_anterior)
        lectura_actual = información['toma_lectura'][0]['lec_actual']
        lecturas_actuales_lista.append(lectura_actual)
        inf_estratos = información['estrato']
        inf_estratos_lista.append(inf_estratos)
        l = list(zip(lecturas_anteriores_lista,lecturas_actuales_lista))
        total_lectura = [abs(l[i][j-1]-l[i][j]) for i in range(len(l)) for j in range(1,len(l[i]))]
        p_sub = [round(0.15*i,2) for i in range(3,0,-1)]
        [p_sub.insert(j,0.55) for j in range(3,6)]
        usuarios.append(elementos)
    if usuarios == []:
        return 'Sin lecturas'     
    for t,k in zip(total_lectura,inf_estratos_lista):
        if k < 4:
            total_liquidado = round(cargo_basico*(1-p_sub[k-1]) + consumo*t*(1-p_sub[k-1]),2)
            total_liquidado_lista.append(total_liquidado)

            total_subsidio = consumo*t*(1-p_sub[k-1])
            total_subsidio = p_sub[k-1]*(cargo_basico + consumo*t)
            total_subsidio_lista.append(total_subsidio) 
            parte_1 = list(zip(usuarios,total_liquidado_lista))              
        if k >= 4:
            total_liquidado = round(cargo_basico*(1+p_sub[k-1]) + consumo*t*(1+p_sub[k-1]),2)
            total_liquidado_lista.append(total_liquidado)
            parte_1 = list(zip(usuarios,total_liquidado_lista)) 

    return parte_1,sum(total_lectura),round(sum(total_subsidio_lista),2)
print(inforServicio(lectura,tarifa))


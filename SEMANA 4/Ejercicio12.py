#INDEXACIÓN DE MATRICES

#Similar a las listas de Python, las matrices numpy se pueden cortar.

import numpy as np

a = np.array(
    [
        [1,2,3],
        [5,6,7],
        [9,10,11]
    ]
)# Crear la siguiente matriz de rango 2 con forma (3, 4)
#print(a)

# Usar el rebanado para sacar el subconjunto que consiste en las 2 primeras filas
# y las columnas 1 y 2

b = a[:2, 1:3]
print(b)
c = a[1:3, 1:3]
print(c)


d = np.fliplr(a) #Invierte los valores de las filas
print(d)

print(d[2,1]) #Elemento en la posicion(2,1)

a[0,0] = 77 #Poner el valor 77 en la posicion (0,0)
print(a)
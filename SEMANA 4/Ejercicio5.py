#FUNCION FILTER

'''
def mayor_a_cinco(elemento):
    return elemento > 5

tupla=(2,3,4,5,6,7,8,9)
print('Número de elementos de la tupla original',len(tupla))
resultado = tuple(filter(mayor_a_cinco,tupla))
print('Número de elementos de la tupla filtrada',len(resultado))
print(resultado)

'''
pares = []

for i in range(11):
    if i%2 == 0:
        pares.append(i)

print(pares)

tupla = (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19)
resultado = tuple(filter(lambda elemento: elemento % 2 == 0, pares))
print(resultado)




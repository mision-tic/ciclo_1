#FUNCION DE ALL Y ANY

uids = ['B1CD102354','B1CDEF2354']

for uid in uids:
    cond= list()
    #por lo menos dos letras mayusculas en el alfabeto ingles
    cond.append(len(list(filter( lambda x: x.isupper(), list(uid)))) >= 2) 

    #Por lo menos tiene tres dígitos
    cond.append(len(list(filter( lambda x: x.isdigit(), list(uid)))) >= 3)

    #Solamente dígitos alfanuméricos
    cond.append(len(list(filter( lambda x: not(x.isalnum()), list(uid)))) == 0)

    #No existan repetidos
    cond.append(len(uid)==len(set(uid)))

    #Longitud de 10 caracteres
    cond.append(len(uid)==10)

    print(cond)
    print('Válido' if all(cond)==True else 'Inválido')



lectura = {
            '201501001' :{
                           'toma_lectura': [
                                             {
                                               'lec_anterior': 12,
                                               'lec_actual': 60,
                                             }
                                           ],
                          'estrato': 1,
                          'estado': 'inactivo'
                         },
            '201501002' :{
                           'toma_lectura': [
                                            {
                                             'lec_anterior': 2,
                                             'lec_actual': 6,
                                            }
                                            ],
                           'estrato': 2,
                           'estado': 'inactivo'
                         },
            '201501003' :{
                            'toma_lectura': [
                                             {
                                              'lec_anterior': 23,
                                              'lec_actual': 43,
                                             }
                                            ],
                            'estrato': 3,
                            'estado': 'inactivo'
                         },
            '201501004' :{
                            'toma_lectura': [
                                             {
                                              'lec_anterior': 90,
                                              'lec_actual': 120,
                                             }
                                            ],
                            'estrato': 1,
                            'estado': 'inactivo'
                         },
            '201501005' :{
                            'toma_lectura': [
                                             {
                                              'lec_anterior': 1,
                                              'lec_actual': 9,
                                             }
                                            ],
                            'estrato': 1,
                            'estado': 'inactivo'
                         },
            '201564006' :{
                            'toma_lectura': [
                                             {
                                              'lec_anterior': 10,
                                              'lec_actual': 20,
                                             }
                                            ],
                            'estrato': 6,
                            'estado': 'inactivo'
                         }
    }


tarifa = {
           'cargo_basico': 9850,
           'consumo': 800.27
         }


def inforServicio(lectura : dict, tarifa : dict)-> tuple:
    lecturas_anteriores_lista, lecturas_actuales_lista, parte_1 ,total_subsidio_lista= [],[],[],[]
    total_liquidado_lista,  inf_estratos_lista= [], []
    a=list(tarifa.values())
    cargo_basico,consumo = a[0],a[1]
    estados = [información['estado'] for elementos,información in lectura.items()]   
    for elementos,información in lectura.items():
        if información['estado'] == 'inactivo':
            continue 
        if set(estados)=={'inactivo'}:
            return 'Sin lecturas'
        lectura_anterior = información['toma_lectura'][0]['lec_anterior']
        lecturas_anteriores_lista.append(lectura_anterior)
        lectura_actual = información['toma_lectura'][0]['lec_actual']
        lecturas_actuales_lista.append(lectura_actual)
        inf_estratos = información['estrato']
        inf_estratos_lista.append(inf_estratos)
        l = list(zip(lecturas_anteriores_lista,lecturas_actuales_lista))
        total_lectura = [abs(l[i][j-1]-l[i][j]) for i in range(len(l)) for j in range(1,len(l[i]))]
        p_sub = [round(0.15*i,2) for i in range(3,0,-1)]
        [p_sub.insert(j,0.55) for j in range(3,6)]    
    for t,k in zip(total_lectura,inf_estratos_lista):
        if k < 4:
            total_liquidado = cargo_basico*(1-p_sub[k-1]) + consumo*t*(1-p_sub[k-1])
            total_liquidado_lista.append(total_liquidado)
            identidad = (elementos,round(total_liquidado,2))  
            total_subsidio = consumo*t*(1-p_sub[k-1])
            total_subsidio_lista.append(total_subsidio)
            parte_1.append(identidad)                     
        if k >= 4:
            total_liquidado = cargo_basico*(1+p_sub[k-1]) + consumo*t*(1+p_sub[k-1])
            total_liquidado_lista.append(total_liquidado)
            identidad = (elementos,round(total_liquidado,2))  
            parte_1.append(identidad)  

    return parte_1,sum(total_lectura),round(sum(total_subsidio_lista),2) 

print(inforServicio(lectura,tarifa))

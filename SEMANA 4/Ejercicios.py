from functools import reduce
#Problema 1:
#Utilizar la función incorporada map() para crear una función que retorne una lista con la
#longitud de cada palabra (separadas por espacios) de una frase. La función recibe una cadena
#de texto y retornará una lista.

'''def longitud(a:str)->list:
    lista = a.split() 
    resultado = list(map(len,lista))
    return(resultado)
print(longitud('Hola Luis, como estas?'))'''

#--------------------------------------------
'''def longitud_palabras(frase): # Función
    palabra_len = list(map(len, frase.split())) # Longitud de cada palabra
    return palabra_len # Retornar resultado
print(longitud_palabras('Hola Luis, como estas?')) # Prueba de la función''' 


#Problema 2:
#Crear una función que tome una lista de dígitos y devuelva al número al que corresponden.
#Por ejemplo [1,2,3] corresponde a el número ciento veintitrés (123). Utilizar la función
#reduce.

'''def digitos(a: list):
    c = [str(x) for x in a]
    resultado = reduce(lambda acumulador = '', elemento = '': acumulador + elemento,c)
    return(resultado)

print(digitos([4,3,9,2]))'''

#---------------------------------------------------------------------------------

'''def digitos_a_numero(digitos):
    return reduce(lambda x,y:x*10 + y,digitos)
print(digitos_a_numero([4,3,9,2]))'''
 
#Problema 3:
#Crear una función que retorne las palabras de una lista de palabras que comience con una
#letra en específico. Utilizar la función filter.


'''def retornar(a: list, letra: str):
    for element in a:
        resultado = list(filter(lambda element : element[0]==letra,a))
    return resultado
print(retornar(['Perro', 'Gato', 'Pelota', 'Manzana', 'Libro', 'Python'], 'P'))'''

#------------------------------------------------------------------------

'''def filtro_palabras(lista_palabras, letra):
    return list(filter(lambda word:word[0]==letra,lista_palabras))

print(filtro_palabras(['Perro', 'Gato', 'Pelota', 'Manzana', 'Libro', 'Python'], 'P'))'''

#Problema 4:
#Realizar una función que tome una lista de comprensión para devolver una lista de la misma
#longitud donde cada valor son las dos cadenas de L1 y L2 concatenadas con un conector entre
#ellas. Ejemplo: Listas: ['A', 'a'] ['B','b'] Conector: '-' Salida: ['A-a'] ['B-b']. Utilizar la función
#zip.

'''def concatenar(L1: list, L2: list):
    L_1 = ['-'.join(L1)]
    L_2 = ['-'.join(L2)]
    resultado = list(zip(L_1,L_2))
    return L_1, L_2
print(concatenar(['A', 'a'],['B','b']))'''


#--------------------------------------------------------

'''def concatenacion(L1, L2, connector):
    return [word1+connector+word2 for (word1,word2) in zip(L1,L2)]
print(concatenacion(['A', 'a'],['B','b'],'-'))'''

#La función enumarate es otra de las herramientas para manipulación de colecciones de datosl
#en Python. Consultar cuál es su finalidad y una vez teniendo claro su comportamiento,
#resolver los dos siguientes problemas propuestos:

#Problema 5:
#Realizar una función que tome una lista y retorne un diccionario que contenga los valores de
#la lista como clave y el índice como el valor. Utilizar la función enumerate.

'''def clave_y_llaves(a: list):

    diccionario = {claves : indices for indices,claves in enumerate(a)}
        
    return diccionario

print(clave_y_llaves(['a','b','c','d','e']))'''

#------------------------------------------------------------

'''def d_list(L):
    return {key:value for value,key in enumerate(L)}
print(d_list(['a','b','c','d','e']))'''



#Problema 6:
#Realizar una función que retorne el recuento de la cantidad de elementos en la lista cuyo
#valor es igual a su índice. Utilizar la función enumerate.


'''def recuento(a: list):
    lista = []
    for i,valor in enumerate(a):
        if valor == i:
            lista.append(valor)  
    return len(lista)
print(recuento([0,2,2,1,5,5,6,10]))'''

'''def recuento(a: list):
    lista = [valor  for i, valor in enumerate(a) if i == valor ]
    return len(lista)
print(recuento([0,2,2,1,5,5,6,10]))'''

#----------------------------------------------------

'''def count_match_index(L):
    return len([num for count,num in enumerate(L) if num==count])
print(count_match_index([0,2,2,1,5,5,6,10]))
'''

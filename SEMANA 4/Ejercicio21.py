# libreria pandas

import numpy as np
import pandas as pd

# a = np.array([[21,44,25],[24,54,11]])
ventas = pd.Series([12,43,25], index = ['Ene','Feb','Mar'])


#print(ventas)


#print(ventas[0])
#print(ventas['Ene'])
#print(ventas.dtype)
#print(ventas.index)
#print(ventas.values)


ventas.name = 'Ventas 2021'
ventas.index.name = 'Meses'

print(ventas)

print(ventas.axes)
print(ventas.shape)

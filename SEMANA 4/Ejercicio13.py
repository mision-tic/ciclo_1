#También puede mezclar la indexación de enteros con la indexación de sectores.
#  Sin embargo, al hacerlo, se obtendrá una matriz de rango más bajo que la matriz original.
import numpy as np

a = np.array(
    [
        [1,2,3,4],
        [5,6,7,8],
        [9,10,11,12]
    ]
)

print(a)

'''
# Dos formas de acceder a los datos de la fila del medio de la matriz.
# Mezclando la indexación de enteros con rebanadas se obtiene una matriz de rango inferior,
# mientras que usando sólo rebanadas se obtiene un conjunto del mismo rango que el
# La matriz original:
row_r1 = a[1,:]
row_r2 = a[1:2, :]
print(row_r1, row_r1.shape)
print(row_r2, row_r2.shape)
'''
# Podemos hacer la misma distinción al acceder a las columnas de una matriz:
col_l1 = a[:,1]
col_l2 = a[:,1:2]
print(col_l1, col_l1.shape)
print(col_l2, col_l2.shape)

print(col_l1[1,])
print(col_l2[1,:])

#CADENA -> LISTA
'''
cadena = 'grupo 39'
lista = list(cadena)
print(lista)
'''
'''
cadena = 'Hola'
cadena2 = 'Adiós'
num = 15
tupla = tuple(cadena)
tuplaGeneral = (tupla,num,tuple(cadena2))
print(tuplaGeneral)
'''
#CADENA -> CONJUNTO
'''

cadena = 'hhola'
print(set(cadena))
'''
#LISTA -> CADENA
'''
lista = ['h','o','l','a',1,(1,2)]
try: 
    cadena = ''.join(lista)
    print(cadena)
except:
    print('No se puede concatenar tipos de datos dintintos de un string')

convertir_cadena = ''
for x in lista:
    convertir_cadena = convertir_cadena + str(x)
print(convertir_cadena)
'''
#LISTA -> TUPLA
'''
lista = ['h','o','l','a',123]
tupla = tuple(lista)
print(tupla)
'''
#LISTA -> CONJUNTO
'''
lista = ['h','o','l','a',1,2,3,1,2,3]
conjunto = set(lista)
print(conjunto)
'''
#TUPLA -> CADENA
'''
tupla = ('h','o','l','a')
cadena = ''.join(tupla)
print(cadena)

tupla2 = ('hola',123,'mundo')
lista = list(tupla2)
print(lista)

tupla = ('Hola',111,'mundo','Hola')
conjunto = set(tupla) 
conjunto.add(15)
print(conjunto)
'''
'''
cadena = '234594855'
lista = list(cadena)
lista.sort()
print(set(lista))
print(set(cadena))

nuevacadena = set()
for elemento in set(cadena):
    nuevacadena.add(int(elemento))
print(nuevacadena)    
'''

#CONVERSION DE CONJUNTOS
'''
conjunto = {'h','o','l','a'}

cadena = ''.join(conjunto) #convertir conjunto en cadena

print(cadena)
'''
'''
conjunto = {'h','o','l','a'}
tupla = tuple(conjunto) #de conjunto a tupla
print(tupla)
'''
'''
conjunto = {'h','o','l','a','a'}
lista = list(conjunto) #de conjunto a lista
print(lista)
'''
#CONVERSION DE DICCIONARIOS
'''
cadena = 'hola'
diccionario = dict()

for posicion in range(len(cadena)):
    diccionario[posicion]=cadena[posicion]

print(diccionario)

dic_zip = dict(zip(range(len(cadena)),cadena))
print(dic_zip)

Lista = ['h','o','l','a']
dic_lista = zip(range(len(Lista)),Lista)
print(dic_zip)

tupla = 'h','o','l','a'
dic_tupla = zip(range(len(tupla)),tupla)
print(dic_tupla)

conjunto = {'h','o','l','a','a'}
dic_conjunto = dict(zip(range(len(conjunto)),conjunto))
print(dic_conjunto)

conjunto = {'h','o','l','a','a'}
dic_conjunto = dict(zip(conjunto,range(len(conjunto))))
print(dic_conjunto)
'''
'''
diccionario = {0:'h',1:'o',2:'l',3:'a'}
cadena = ''.join(diccionario.values())
print(cadena)
'''

'''
diccionario = {0:'h',1:'o',2:'l',3:'a'}
tuplavalores = tuple(diccionario.values())
print(tuplavalores)
tuplallaves = tuple(diccionario.keys())
print(tuplallaves)

tuplaitems = tuple(diccionario.items())
print(tuplaitems)
'''
'''
diccionario = {0:'h',1:'o',2:'l',3:'a'}
listavalores = list(diccionario.values())
print(listavalores)
listallaves = list(diccionario.keys())
print(listallaves)

listaitems = list(diccionario.items())
print(listaitems)
'''
'''
diccionario = {0:'h',1:'o',2:'l',3:'a',4:'a'}
conjuntokeys = set(diccionario.keys())
print(conjuntokeys)

conjuntovalues = set(diccionario.values())
print(conjuntovalues)

conjuntoitems = set(diccionario.items())
print(conjuntoitems)

con = {4,7,9,4,7}
print(con)

'''

numero = 1,2,3,4,5,6,7
numero_1 = 'uno','dos','tres','cuatro','cinco','seis','siete'

resultado_tupla = tuple(zip(numero,numero_1))
print(resultado_tupla)

resultado_dict = dict(zip(numero,numero_1))
print(resultado_dict)

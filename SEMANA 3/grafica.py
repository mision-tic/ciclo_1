import numpy as np

angle = -np.pi/2
expand = 1/np.exp(1)
direct_xvectors3=np.array([matrix_expand(expand,angle,[1/2,0])[0],matrix_expand(expand,angle,[-1/2,0])[0],matrix_expand(expand,angle,[0,1/2])[0],matrix_expand(expand,angle,[0,-1/2])[0]])
direct_yvectors3=np.array([matrix_expand(expand,angle,[1/2,0])[1],matrix_expand(expand,angle,[-1/2,0])[1],matrix_expand(expand,angle,[0,1/2])[1],matrix_expand(expand,angle,[0,-1/2])[1]])

vectors_around(fz_2,direct_xvectors3,direct_yvectors3,ax_2,'r')
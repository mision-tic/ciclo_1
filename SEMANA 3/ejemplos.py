

creditos = {
    '2019041209845' :
                     {
                         'nombres': 'Elias',
                         'apellidos': 'Diaz Lopez',
                         'est_credito': 'Activo',
                         'credito': 
                                  [
                                     {
                                        'id_credito': 'C0335501',
                                           'cuotas': 3,
                                          'valor': 87558,
                                          'interes': 0.020,
                                           'cuo_vencidas': 1,
                                          'cuo_pagadas':2
                                     }
                                  ]
                    },
    '2015020192098' :
                     {
                        'nombres': 'Juan',
                        'apellidos': 'Arias Ruiz',
                        'est_credito': 'Activo',
                        'credito':
                                  [
                                      {
                                          'id_credito': 'C0198238',
                                          'cuotas': 24,
                                          'valor': 3066936.00,
                                          'interes': 0.020,
                                          'cuo_vencidas': 8,
                                          'cuo_pagadas':10
                                      }
                                  ]
                     },
    '2018015647382' :
                     { 
                         'nombres': 'Luis Antonio',
                         'apellidos': 'Lopez Rueda',
                         'est_credito': 'Activo',
                         'credito': 
                                   [
                                      {
                                           'id_credito': 'C0013453',
                                           'cuotas': 60,
                                           'valor': 87558500,
                                           'interes': 0.020,
                                           'cuo_vencidas': 30,
                                           'cuo_pagadas':7
                                      }
                                   ]
                     },
    '2021015647382' :
                     { 
                         'nombres': 'Laura Camila',
                         'apellidos': 'Castillo Sanchez',
                         'est_credito': 'Activo',
                         'credito': 
                                   [
                                      {
                                           'id_credito': 'C0013453',
                                           'cuotas': 32,
                                           'valor': 25460000,
                                           'interes': 0.020,
                                           'cuo_vencidas': 6,
                                           'cuo_pagadas':20
                                      }
                                   ]
                     },
    '2022015647382' :
                     { 
                         'nombres': 'Andres Camilo',
                         'apellidos': 'Salamanca Ruiz',
                         'est_credito': 'Activo',
                         'credito': 
                                   [
                                      {
                                           'id_credito': 'C0013453',
                                           'cuotas': 15,
                                           'valor': 1500000,
                                           'interes': 0.020,
                                           'cuo_vencidas': 7,
                                           'cuo_pagadas':4
                                      }
                                   ]
                     },
    '2012015647382' :
                     { 
                         'nombres': 'Cristian Felipe',
                         'apellidos': 'Peña Rodriguez',
                         'est_credito': 'Pagado',
                         'credito': 
                                   [
                                      {
                                           'id_credito': 'C0013453',
                                           'cuotas': 47,
                                           'valor': 15689500,
                                           'interes': 0.020,
                                           'cuo_vencidas': 18,
                                           'cuo_pagadas':10
                                      }
                                   ]
                     }
      
            }



def calcularInforme(creditos : dict)->list:
    valor_intereses_l, vlr_pagar_l, vlr_saldo_l  = [], [], []
    numero_cuotas_l , valor_prestamo_l, valor_interes_l, valor_cuotas_l = [], [], [], []
    num_cuotas_pagadas_l, num_cuotas_elaboradas_l, num_cuotas_vencidas_l = [],[],[]
    usuarios_l, est_credito_l = [], []
    v_p , v_m , v_e = 0,0,0
    for id, informacion in creditos.items():
        if informacion['est_credito'] == 'Pagado':
            continue
        numero_cuotas = informacion['credito'][0]['cuotas']
        numero_cuotas_l.append(numero_cuotas)
        valor_prestamo = informacion['credito'][0]['valor']
        valor_prestamo_l.append(valor_prestamo)
        valor_interes = informacion['credito'][0]['interes']
        valor_interes_l.append(valor_interes)
        valor_cuotas = valor_prestamo/numero_cuotas
        valor_cuotas_l.append(valor_cuotas)
        cuotas_pagadas = informacion['credito'][0]['cuo_pagadas']
        num_cuotas_pagadas_l.append(cuotas_pagadas)
        cuotas_vencidas = informacion['credito'][0]['cuo_vencidas']
        num_cuotas_vencidas_l.append(cuotas_vencidas)
        cuotas_elaboradas = numero_cuotas - cuotas_vencidas - cuotas_pagadas
        num_cuotas_elaboradas_l.append(cuotas_elaboradas)
        est_credito = informacion['est_credito']
        est_credito_l.append(est_credito)
        usuario = id + ':' + informacion['nombres'][0:2] + ':' + informacion['apellidos'][0:2]
        usuarios_l.append(usuario)   
    for i in range(0,len(usuarios_l)):
        for k in range(0,numero_cuotas_l[i]):      
            if k == 0:
                valor_intereses = valor_prestamo_l[i] * valor_interes_l[i]
                valor_intereses_l.insert(k,valor_intereses)
                vlr_pagar = valor_intereses + valor_cuotas_l[i]
                vlr_pagar_l.insert(k,vlr_pagar)
                vlr_saldo = valor_prestamo_l[i] - valor_cuotas_l[i]
                vlr_saldo_l.insert(k,vlr_saldo)
            else:
                valor_intereses = (vlr_saldo_l[k-1])*valor_interes_l[i]
                valor_intereses_l.insert(k,valor_intereses)
                vlr_pagar = valor_intereses_l[k] + valor_cuotas_l[i]
                vlr_pagar_l.insert(k,vlr_pagar)
                vlr_saldo = (vlr_saldo_l[k-1])-valor_cuotas_l[i]
                vlr_saldo_l.insert(k,vlr_saldo)
        for j in range(0,num_cuotas_pagadas_l[i]):
            v_p = v_p + vlr_pagar_l[j]
        for h in range(num_cuotas_pagadas_l[i],num_cuotas_pagadas_l[i]+num_cuotas_vencidas_l[i]):
            v_m = v_m + vlr_pagar_l[h]
        for t in range(num_cuotas_vencidas_l[i]+num_cuotas_pagadas_l[i],numero_cuotas_l[i]):
            v_e = v_e + vlr_pagar_l[t]       

    return [':'.join(usuarios_l), (round(v_p,2) , round(v_m,2), round(v_e,2))]

        
print(calcularInforme(creditos))    
'''
Usuario = {'a':{'est_credito':'Pagado'},'b':{'est_credito':'Pagado'},'c':{'est_credito':'Activo'}}

lista = []
usuarios_l = []
for letra, informacion in Usuario.items():
    if informacion['est_credito'] == 'Pagado':
        continue
    usuario = letra + ':' + informacion['est_credito']
    usuarios_l.append(usuario)
    est_credito = informacion['est_credito']
    lista.append(est_credito)

print(usuarios_l)
print(lista)
'''













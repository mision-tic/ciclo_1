#WHILE Y FOR
'''
x = 0
x = x + 1

print(x)
 

y = 10
y = y-1
print(y)

n=5

while n > 0:
    print(n)
    n = n-1
print("Finalizo")


def factorial(n: int)->int:
    resultado = 1
    numero_actual = 2
    while numero_actual <= n:
        resultado = resultado*numero_actual
        numero_actual += 1
        #print(resultado)
    return resultado    
print(factorial(7))

i = 1
while i != 10:
    print(i)
    i = i + 1
print('Terminó')    

i = 1
while i < 10:
    print(i)
    i += 1
print("Terminó")    

promedio, total, contar = 0.0, 0, 0

print("Introduzca la nota de un estudiante (-1 para salir): ")
grado = int(input())
while grado != -1:
    total = total + grado
    contar = contar +1
    print("Introduzca la nota del estudiante (-1 para salir): ")
    grado = int(input())
promedio = total / contar
print("El promedio de la nota de los estudiantes es: ", promedio)


variable = 10
while variable > 0:
    print("valor de la variable", variable)
    variable = variable -1
    if variable == 5:
        break

variable = 10
while variable > 0:
    variable = variable - 1
    if variable == 5:
        continue
    print("valor de la variable es: ", variable)


for x in range(0,5):
    print("estamos en la iteración: "+str(x))
    #print("estamos en la iteración: ", x)

for y in range(0,10,2):
    print("estamos en la iteración: "+str(y))
    #print("estamos en la iteración: ", x)    

for j in range(10,0,-2):
    print("estamos en la iteración: "+str(j))
    #print("estamos en la iteración: ", x)

'''
oracion = 'Mery entiende muy bien python'
frases = oracion.split() #' ,'
print("la oracion a analizar es: ", oracion, '\n')
print(frases)


for palabra in range(len(frases)):
    print('palabra: {}, la palabra en la posición {}'.format(frases[palabra],palabra))

'''
'''
datos_basicos ={
    'Nombres': 'Fernando',
    'Apellidos': 'Caballero García',
    'Cedula': '11009834895',
    'Lugar de nacimiento': '13/12/1980',
    'Fecha de nacimeinto': 'Bucaramanga',
    'Nacionalidad': 'Colombiano',
    'Estando civil': 'Soltero'
}
'''
clave = datos_basicos.keys()
print(clave)

valor = datos_basicos.values
print(valor)
'''
datos = datos_basicos.items()
print(datos)
'''
for clave_1,valor_1 in datos:
    print(clave_1, ':', valor_1)

for clave_1 in datos_basicos.items():
    print(clave_1)



frutas = {
    'fresa': 'roja',
    'limon': 'verde',
    'papaya': 'naranja',
    'manzana': 'amarillo',
    'Guayaba': 'rosa'

}

for llave in frutas:
    print(llave,' es de color', frutas[llave])


db_connection = '127.0.0.1','5432','root', 'nomina' #si no se pone parentesis igual se conisdera tupla
print(db_connection)

for parametro in db_connection:
    print(parametro)
else:
    print('El comando de postgresSQL es: $ psql -h {} -p {} -u {} -d {}'.format(db_connection[0],db_connection[1],db_connection[2],db_connection[3]))

'''
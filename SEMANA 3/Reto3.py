


creditos = {
    '2015020192098' :
                     {
                        'nombres': 'Juan',
                        'apellidos': 'Arias Ruiz',
                        'est_credito': 'Activo',
                        'credito':
                                  [
                                      {
                                          'id_credito': 'C0198238',
                                          'cuotas': 24,
                                          'valor': 3066936.00,
                                          'interes': 0.020,
                                          'cuo_vencidas': 8,
                                          'cuo_pagadas':10
                                      }
                                  ]
                     },
    '2018015647382' :
                     { 
                         'nombres': 'Luis Antonio',
                         'apellidos': 'Lopez Rueda',
                         'est_credito': 'Activo',
                         'credito': 
                                   [
                                      {
                                           'id_credito': 'C0013453',
                                           'cuotas': 60,
                                           'valor': 87558500,
                                           'interes': 0.020,
                                           'cuo_vencidas': 30,
                                           'cuo_pagadas':7
                                      }
                                   ]
                     }
            }
    
'''  '2019041209845' :
                     {
                         'nombres': 'Elias',
                         'apellidos': 'Diaz Lopez',
                         'est_credito': 'Activo',
                         'credito': 
                                  [
                                     {
                                        'id_credito': 'C0335501',
                                           'cuotas': 3,
                                          'valor': 87558,
                                          'interes': 0.020,
                                           'cuo_vencidas': 1,
                                          'cuo_pagadas':2
                                     }
                                  ]
                    }'''



'''
creditos = {
    '2015020192098' :
                     {
                        'nombres': 'Juan',
                        'apellidos': 'Arias Ruiz',
                        'est_credito': 'Activo',
                        'credito':
                                  [
                                      {
                                          'id_credito': 'C0198238',
                                          'cuotas': 24,
                                          'valor': 3066936.00,
                                          'interes': 0.020,
                                          'cuo_vencidas': 8,
                                          'cuo_pagadas':10
                                      }
                                  ]
                     }
}
    
'''
def calcularInforme(creditos : dict) -> list:
    val_pagados, val_en_mora, val_elaborados  = 0, 0, 0
    valor_intereses_l, vlr_pagar_l, vlr_saldo_l  = [], [], []
    numero_cuotas_l , valor_prestamo_l, valor_interes_l, valor_cuotas_l = [], [], [], []
    c_v_l, c_p_l, c_e_l = [], [],[]
    for id, informacion in creditos.items():
        numero_cuotas = informacion['credito'][0]['cuotas']
        numero_cuotas_l.append(numero_cuotas)
        valor_prestamo = informacion['credito'][0]['valor']
        valor_prestamo_l.append(valor_prestamo)
        valor_interes = informacion['credito'][0]['interes']
        valor_interes_l.append(valor_interes)
        valor_cuotas = round(valor_prestamo/numero_cuotas,2)
        valor_cuotas_l.append(valor_cuotas)
        cuotas_vencidas = informacion['credito'][0]['cuo_vencidas']
        
        cuotas_pagadas = informacion['credito'][0]['cuo_pagadas']
        cuotas_elaboradas = numero_cuotas - cuotas_vencidas - cuotas_pagadas
        for k in range(0,numero_cuotas):
            if k == 0:
                valor_intereses = valor_prestamo * valor_interes
                valor_intereses_l.append(valor_intereses)
                vlr_pagar = valor_intereses + valor_cuotas
                vlr_pagar_l.append(vlr_pagar)
                vlr_saldo = valor_prestamo - valor_cuotas
                vlr_saldo_l.append(vlr_saldo)
            else:
                valor_intereses = (vlr_saldo_l[k-1])*valor_interes
                valor_intereses_l.append(valor_intereses)
                vlr_pagar = valor_intereses + valor_cuotas
                vlr_pagar_l.append(vlr_pagar)
                vlr_saldo = (vlr_saldo_l[k-1])-valor_cuotas
                vlr_saldo_l.append(vlr_saldo)
            
        for j in range(cuotas_pagadas):
            val_pagados = round(val_pagados + vlr_pagar_l[j],2)
        for h in range(cuotas_pagadas,cuotas_vencidas + cuotas_pagadas):
            val_en_mora = round(val_en_mora + vlr_pagar_l[h],2)
        for t in range(cuotas_vencidas + cuotas_pagadas,numero_cuotas):
            val_elaborados = round(val_elaborados + vlr_pagar_l[t],2)

    return [id + ':' + informacion['nombres'][0:2] + ':' + informacion['apellidos'][0:2],(val_pagados,val_en_mora,val_elaborados)]
        
print(calcularInforme(creditos))

#primer_elemento = id + ':' + informacion['nombres'][0:2] + ':' + informacion['apellidos'][0:2]
# debo retornar la suma de los valores pagados, valores en mora y valores elaborados
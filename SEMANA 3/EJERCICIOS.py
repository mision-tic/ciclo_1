#1). Escribir un programa que solicite ingresar 10 notas de alumnos y nos informe cuántos tienen notas mayores o 
# iguales a 7 y cuántos menores.

#FOR
'''
Notas_mayores_a_7 = list()
Notas_menores_a_7 = list()
Notas = list()

for x in range(10):
    notas = int(input('Ingrese las notas de los estudiantes: '))    
    if notas >= 7:
        Notas_mayores_a_7.append(notas)
        a = len(Notas_mayores_a_7)
    else:
        Notas_menores_a_7.append(notas)
        b = len(Notas_menores_a_7)
print('Hay',a, 'notas mayores o iguales a 7')
print('Hay',b, 'notas menores a 7')
'''
#WHILE
'''
i = 1
Notas_mayores_a_7 = list()
Notas_menores_a_7 = list()
while i < 11:
    notas = int(input('Ingrese las notas de los estudiantes: '))
    if notas >= 7:
        Notas_mayores_a_7.append(notas)
        a = len(Notas_mayores_a_7)
    else:
        Notas_menores_a_7.append(notas)
        b = len(Notas_menores_a_7)
    i += 1
print('Hay',a, 'notas mayores o iguales a 7')
print('Hay',b, 'notas menores a 7')
'''



#2). Se ingresan un conjunto de n alturas de personas por teclado. Mostrar la altura promedio de las personas.
'''
promedio_altura, total, contar = 0.0, 0, 0

print("Introduzca la altura de la persona en centimetros ('0' para terminar): ")
altura = int(input())
while altura != 0:
    total = total + altura
    contar = contar +1
    print("Introduzca la altura de la persona en centimetros ('0' para terminar): ")
    altura = int(input())
promedio_altura = total / contar
print("El promedio de las alturas de las personas es: ", promedio_altura,'centimetros')

'''

#3). En una empresa trabajan n empleados cuyos sueldos oscilan entre $100 y $500, 
# realizar un programa que lea los sueldos que cobra cada empleado e informe cuántos 
# empleados cobran entre $100 y $300 y cuántos cobran más de $300. Además el programa 
# deberá informar el importe que gasta la empresa en sueldos al personal.
'''
entre_100_y_300 = list()
más_de_300 = list()
menos_300 = list()
importe, contar_M, contar_m = 0, 0, 0
print('Ingrese el sueldo de cada empleado: ')
sueldo = int(input())
while sueldo != 0:
    importe = importe + sueldo
    if sueldo >= 100 and sueldo <= 300:
        contar_M = contar_M + 1
    elif sueldo > 300:
        contar_m = contar_m + 1
    elif sueldo < 100:
        menos_300.append(sueldo)
    print('Ingrese el sueldo de cada empleado: ')
    sueldo = int(input())   
print('Hay',contar_M, 'empleados que cobran entre $100 y $300 y hay',contar_m,'empleados que cobran más de $300, además, el importe es de $',importe)
'''

#4). Realizar un programa que imprima 25 términos de la serie 11 - 22 - 33 - 44, etc. (No se ingresan valores por teclado)




#5). Mostrar los múltiplos de 8 hasta el valor 500. Debe aparecer en pantalla 8 - 16 - 24, etc.
'''

Multiplos_8 = list()

while x in range(8,500):
    if x % 8 == 0:
        Multiplos_8.append(x)
        L = tuple(Multiplos_8)
    else:
        continue
while y in range(0,len(L)):
    print(Multiplos_8[y])

'''

# 6). Realizar un programa que permita cargar dos listas de 15 valores cada una. 
# Informar con un mensaje cuál de las dos listas tiene un valor acumulado mayor
#  (mensajes "Lista 1 mayor", "Lista 2 mayor", "Listas iguales")

#FOR
'''
Lista_1 = list()
for x in range(15):
    print('Ingresa los valores de la lista 1: ')
    Elementos_1 = input()
    Lista_1.append(Elementos_1)
Lista_2 = list()
for y in range(15):
    print('Ingresa los valores de la lista 2: ')
    Elementos_2 = input()
    Lista_1.append(Elementos_2)
for z in range(0,15):
    if Lista_1[z] == Lista_2[z]:
        print('Lista 1 mayor')

'''

#WHILE
'''
i = 1
j = 1
Lista_1 = list()
Lista_2 = list()
while i < 16:
    print('Ingresa un valor de la lista 1: ')
    Elementos_1 = input()
    Lista_1.append(Elementos_1)
    i += 1
while j < 16:
    print('Ingresa los valores de la lista 2: ')
    Elementos_2 = input()
    Lista_1.append(Elementos_2)
    j += 1
Lista_T_1 = tuple(Lista_1)
Lista_T_2 = tuple(Lista_2)

if Lista_T_1 < Lista_T_2:
    print('Lista 2 mayor')
elif Lista_T_2 < Lista_T_1:
    print('Lista 1 mayor')
else:
    print('Listas iguales')
'''
'''
#WHILE
i = 1
j = 1
while i < 2:
    print('Ingresa la lista 1: ')
    Lista_1 = list(input())
    i=i+1
while j < 2:
    print('Ingresa la lista 2: ')
    Lista_2 = list(input())
    j=j+1

Lista_T_1 = tuple(Lista_1)
Lista_T_2 = tuple(Lista_2)

if Lista_T_1 < Lista_T_2:
    print('Lista 2 mayor')
elif Lista_T_2 < Lista_T_1:
    print('Lista 1 mayor')
elif Lista_T_2 == Lista_T_1:
    print('Listas iguales')
'''

'''
if len(Lista_T_1) == 15 and len(Lista_T_2) == 15:
    if Lista_T_1 < Lista_T_2:
        print('Lista 2 mayor')
    elif Lista_T_2 < Lista_T_1:
        print('Lista 1 mayor')
    elif Lista_T_2 == Lista_T_1:
        print('Listas iguales')
else:
    print('sdf')

[ [a, [10,[z,s]], [1,ccc],[-3,[a,32]]
[1, [[q,s],[-1,a]]

[1, 2.5, 'DevCode',[3,6], 3,'y','8a',1+45,'retr',True,False,1,2,3,'yyyyy']

[9,13,'tyu','y',True,'kbskfsdf',[4,5,4,6],'ttt',False,435435-35435,56,'rty','er',65,'www']
'''
# 7). Desarrollar un programa que permita cargar n números enteros y 
# luego nos informe cuántos valores fueron pares y cuántos impares.
# Emplear el operador “%” en la condición de la estructura condicional 
# este operador retorna el resto de la división de dos valores, por ejemplo 11%2 retorna un 1

'''
numeros_pares, numeros_impares, contar = 0,0,0

print('Ingresa un número entero: ')
entero = int(input())
while entero != -1:
    if entero % 2 == 0:
        numeros_pares += 1
    elif entero % 2 == 1:
        numeros_impares += 1
    print('Ingresa un número entero: ')
    entero = int(input())
print('Hay',numeros_pares, 'números pares y',numeros_impares,'números impares.' )
'''
# 8). Confeccionar un programa que lea n pares de datos, cada par de datos corresponde a la 
# medida de la base y la altura de un triángulo. El programa deberá informar: 
# a) De cada triángulo la medida de su base, su altura y su superficie.
# b) La cantidad de triángulos cuya superficie es mayor a 12.

#INCOMPLETO
'''
c_superficies = 0

print('¿Cuántos datos vas a ingresar?: ')
dato = int(input())
for x in range(0,dato):
    print('Ingresa los datos: ')
    dato = tuple(input())
    base = int(dato[0])
    altura = int(dato[1])
    superficie = (base*altura)/2
    print(type(base))
    if superficie > 12:
        c_superficies += 1
    print('La base del triangulo es:',base,';la altura es:',altura,'; y la superficie es:',superficie)
    print('Ingresa una pareja de datos: ')
    dato = tuple(input())

'''

# 9). Desarrollar un programa que solicite la carga de 10 números e imprima la suma de los últimos 5 valores ingresados.
'''
lista = 0
lista_numeros = list()
for x in range(10):
    Numeros = int(input('Ingresa el número: '))
    lista_numeros.append(Numeros)
for y in range(len(lista_numeros)-5,len(lista_numeros)):
    lista = lista + lista_numeros[y]
print(lista)
'''

# 10). Desarrollar un programa que muestre la tabla de multiplicar del 5 (del 5 al 50)
'''
for x in range(1,11):
    multi = 5*x
    print('5 x',x,'=',multi)
'''

# 11). Confeccionar un programa que permita ingresar un valor del 1 al 10 y nos muestre
#  la tabla de multiplicar del mismo (los primeros 12 términos)
'''
print('Ingrese un número del 1 al 10: ')
numero = int(input())
for x in range(1,13):
    multi = numero*x
    print(numero,'x',x,'=',multi)
'''
# 12). Realizar un programa que lea los lados de n triángulos, e informar:
#  a) De cada uno de ellos, qué tipo de triángulo es: equilátero (tres lados iguales),
#  isósceles (dos lados iguales), o escaleno (ningún lado igual) 
# b) Cantidad de triángulos de cada tipo.
'''
print('Cuántos datos vas a ingresar?: ')
datos = int(input())
tri_equilatero = 0
tri_isosceles = 0
tri_escaleno = 0
for x in range(1,datos+1):
    print('Ingrese los lados del triangulo en forma de tupla: ')
    lados = input()
    if lados[1]==lados[3]==lados[5]:
        print('El triangulo es equilatero')
        tri_equilatero += 1 
    if lados[1]==lados[3] and lados[1] != lados[5]:
        print('El triangulo es isósceles')
        tri_isosceles += 1
    elif lados[1]==lados[3] and lados[3] != lados[5]:
        print('El triangulo es isósceles')
        tri_isosceles += 1
    if lados[5]==lados[1] and lados[1] != lados[3] :
        print('El triangulo es isósceles')
        tri_isosceles += 1
    if lados[1]!=lados[3]!=lados[5]:
        print('El triangulo es escaleno')
        tri_escaleno += 1
print('Hay',tri_equilatero,'triangulos equilateros;',tri_isosceles,'triangulos isosceles, y',tri_escaleno,'triangulos escalenos.')

'''

# 13). Escribir un programa que pida ingresar coordenadas (x,y) que representan puntos en el plano.
#Informar cuántos puntos se han ingresado en el primer, segundo, tercer y cuarto cuadrante. 
# Al comenzar el programa se pide que se ingrese la cantidad de puntos a procesar.

#INCOMPLETO
'''
print('Cuántas coordenadas va a ingresar?: ')
coordenadas = int(input())
primer_cuadrante, segundo_cuadrante, tercer_cuadrante, cuarto_cuadrante = 0, 0, 0, 0

for x in range(0,coordenadas):
    print('Ingrese la coordenada en forma de tupla (x,y): ')
    punto = tuple(input())
    if punto[0]>0 and punto[1]>0:
        primer_cuadrante += 1
    elif punto[1]<0 and punto[3]>0:
        segundo_cuadrante += 1
    elif punto[1]<0 and punto[3]<0:
        tercer_cuadrante += 1
    elif punto[1]>0 and punto[3]<0:
        cuarto_cuadrante += 1
print('Hay',primer_cuadrante,'puntos en el primer cuadrante;',segundo_cuadrante,'puntos en el segundo cuadrante'
,tercer_cuadrante, ',puntos en el tercer cuadrante ,y hay ',cuarto_cuadrante,'en el cuarto cuadrante.')

'''

# 14). Se realiza la carga de 10 valores enteros por teclado. Se desea conocer:
#a) La cantidad de valores ingresados negativos.
#b) La cantidad de valores ingresados positivos.
#c) La cantidad de múltiplos de 15.
#d) El valor acumulado de los números ingresados que son pares.
'''
negativos, positivos, multiplos_5, pares  = 0, 0, 0, 0

for x in range(0,10):
    print('Ingrese el número: ')
    numero = int(input())
    if numero < 0 :
        negativos += 1
    if numero%5 == 0:
        multiplos_5 += 1
    if numero > 0:
        positivos += 1
    if numero == 0:
        print('0 no es ni positivo ni negativo')
    if numero % 2 ==0:
        pares = pares + numero
print('La cantidad de números negativos es: ',negativos,'y la de positivos es: ',positivos)
print('La cantidad de números multiplos de 5 es: ',multiplos_5)
print('El valor acumulado de los números que son pares es: ',pares)

'''

# 15). Se cuenta con la siguiente información:
#Las edades de 5 estudiantes del turno mañana.
#Las edades de 6 estudiantes del turno tarde.
#Las edades de 11 estudiantes del turno noche.
#Las edades de cada estudiante deben ingresarse por teclado.
#a) Obtener el promedio de las edades de cada turno (tres promedios)
#b) Imprimir dichos promedios (promedio de cada turno)
#c) Mostrar por pantalla un mensaje que indique cuál de los tres turnos tiene un promedio de edades mayor.

'''
e_turno_m, e_turno_t, e_turno_n = 0, 0, 0

for i in range(5):
    print('Ingrese las edades del turno de la mañana: ')
    edad_m = int(input())
    e_turno_m += edad_m 
    promedio_m = e_turno_m/5
for j in range(6):
    print('Ingrese las edades del turno de la tarde: ')
    edad_t = int(input())
    e_turno_t += edad_t 
    promedio_t = e_turno_t/6
for k in range(11):
    print('Ingrese las edades del turno de la noche: ')
    edad_n = int(input())
    e_turno_n += edad_n 
    promedio_n = e_turno_n/11

print('El promedio de las edades del turno de la mañana es: ',promedio_m)
print('El promedio de las edades del turno de la tarde es: ',promedio_t)
print('El promedio de las edades del turno de la noche es: ',promedio_n)

if promedio_m > promedio_t  and promedio_m > promedio_n:
    print('El turno de la mañana tiene un promedio de edades mayor.')
if promedio_t > promedio_m and promedio_t > promedio_n:
    print('El turno de la tarde tiene un promedio de edades mayor.')
if promedio_n > promedio_m and promedio_n > promedio_t:
    print('El turno de la noche tiene un promedio de edades mayor.')

'''

# CADENAS DE CARACTERES

#16). Cargar una oración por teclado. Mostrar luego cuantos espacios en blanco se ingresaron. 
# Tener en cuenta que un espacio en blanco es igual a " ", en cambio una cadena vacía es ""
'''
oracion = input('Ingrese una oración: ')

espacios_blanco = oracion.count(" ")
print('El número de espacios es: ',espacios_blanco)

'''
#17). Ingresar una oración que pueden tener letras tanto en mayúsculas como minúsculas.
#  Contar la cantidad de vocales. Crear un segundo string con toda la oración en minúsculas
#  para que sea más fácil disponer la condición que verifica que es una vocal.
'''
oracion = input('Ingrese una oración: ')
vocales = 0
x = 0
while x < len(oracion):
    if oracion[x]=='a' or oracion[x]=='e' or oracion[x]=='i' or oracion[x]=='o' or oracion[x]=='u':
        vocales += 1
    if oracion[x]=='A' or oracion[x]=='E' or oracion[x]=='I' or oracion[x]=='O' or oracion[x]=='U':
        vocales += 1
    x +=1
print('La cantidad de vocales es: ',vocales)

oracion_min = oracion.lower() #Vuelve todo minuscula
print(oracion_min)
'''

# 18). Solicitar el ingreso de una clave por teclado y almacenarla en una cadena de caracteres.
#  Controlar que el string ingresado tenga entre 10 y 20 caracteres para que sea válido, 
# en caso contrario mostrar un mensaje de error.
'''
clave = input('Ingrese una clase: ')

if len(clave) > 20 or len(clave) < 10:
    print('Error')
else:
    print('Correcto')

'''
'''
t = 'a','b','c','d','e'
print(t)
print(type(t))
t = ('esto es una cadena','a','b','c','d','e')
print(t)
t1 = ('a',)
print(type(t1))
t2 = ('a')
print(type(t2))
'''


t = tuple()
print(t)
t = tuple('lupines')
print(t)
print(t[3:5])
print(t[3:6])
# t[0] = 'J' # No se puede asignar valor a una tupla
'''
print((0,1,2000000) < (0,3,4))
print((0,2000000,1) < (0,3,4))
print((0,3,1) < (0,3,4))
'''

txt = 'but soft what light windows break in'
palabra = txt.split()
print(palabra)
# print(type(palabra))

'''l = list()

for subcadena in palabra:
    l.append((len(subcadena),subcadena,subcadena[0]))

print(l)

l.sort(reverse=True)
print(l)

res = list()
res_num = list()

for longitud, palabra, letra in l:
    res.append(palabra)
    res_num.append(longitud)

print(res)
print(res_num)'''
# print(type(res[1]))

#CONJUNTOS

'''

s = {1,2,3,4}
print(s)

s = {True, 3.14, None, False, "Hola mundo", (1,2)}
print(s)

s = {[1,2]}
print(s)



s = {}
print(type(s))

s = set()

print(type(s))

s1 = set([1,2,3])
s2 = set(range(5))

print(s1)
print(s2)

print(list({1,2,3,4}))
print(set([1,2,3,4]))

c = {1,2+5,3,4,9,3,5}

print(c)

a = set("hola pythonista")
print(a)

mi_conjunto = {1,3,2,9,3,1}
print(mi_conjunto)

for numero in mi_conjunto:
    print(numero)

set_mutable1 = set([4,3,11,7,5,2,1,4])
#set_mutable1 = {4,3,11,7,5,2,1,4}
print(set_mutable1)

set_mutable1.add(22.00001)
print(set_mutable1)

set_mutable1.clear()
print(set_mutable1)

set_mutable = set([4.0,"carro",True])
otro_set_mutable = set_mutable.copy()
print(otro_set_mutable)

set_mutable1 = set([4,3,7,11,7,5,2,1,4])
set_mutable2 = set([11,5,9,2,4,8])
print(set_mutable1)
print(set_mutable2)
print(set_mutable1.difference(set_mutable2))
print(set_mutable2.difference(set_mutable1))

a = {1,2,3,4}

b = {2,3}

c = a-b

print(c)

print({1,2,3}=={3,4,1})

proyecto1 = {"python","zope2","ZODB3","pytz"}
print(proyecto1)
proyecto2 = {"python","plone","diazo"}
print(proyecto2)

proyecto1.difference_update(proyecto2)
print(proyecto1)

set_mutable1 = set([4,3,7,11,7,5,2,1,4])

set_mutable2 = set([11,5,9,2,4,8])

set_mutable1.intersection(set_mutable2)
print(set_mutable1)


proyecto1 = {"python","zope2","ZODB3","pytz"}
#print(proyecto1)
proyecto2 = {"python","plone","diazo"}
#print(proyecto2)
proyecto3 = {"python","django","django-filter"}

proyecto1.intersection_update(proyecto2,proyecto3)

print(proyecto1)

set_mutable1 = set([4,3,7,11,7,5,2,1,4])
set_mutable2 = set([11,5,9,2,4,8])

set_mutable1.isdisjoint(set_mutable2)
print(set_mutable1)


set_mutable1 = set([4,3,7,11,7,5,2,1,4])
set_mutable2 = set([11,5,9,2,4,8])
set_mutable3 = set([11,5,2,4])
print(set_mutable1)
print(set_mutable2)
print(set_mutable3)

print(set_mutable2.issubset(set_mutable1)) #No es subconjunto

print(set_mutable3.issubset(set_mutable1)) #Si es subconjunto
'''

paquetes = {"python","zope","plone","django"}
#print("El valor aleatorio es: ", paquetes.pop())
paquetes.remove("django")
print(paquetes)
'''
contador = contador + 1
acumuladores = acumuladores + variable
'''

lista = [1, 2.5, 'DevCode',[7,9], 3]
'''
print(lista[0])
print(lista[1])
print(lista[2])
print(lista[3])
print(lista[3][0])
print('hicimos este cambio -> ', lista[1:4])
print(lista[1:6])
print(lista[1:6:1])
print(lista[1:6:2])
print(lista[1:6:3])
print(lista[1:6:4])
'''

'''
nombre = 'Juan'
edad = 26
lista = [nombre,edad]
print(lista)
nombre = 'Jorge'
print(lista)
'''

'''
nombres = ['Ana','Bernardo']
edades = [23,35]
lista = [nombres,edades]
print(lista)
nombres += ['Cristina']
print(lista)
'''

'''
factura = ['pan','huevos',200,350]
print(len(factura))
print(factura[-1])
'''
#.append agrega valores a la lista

versiones_phone = [2.5,3.6,4,5]
versiones_phone.append(6)
# print(versiones_phone)

#OTRA FORMA DE AGREGAR VALORES A LA LISTA

versiones_phone += [7]
# print(versiones_phone)

#.COUNT CUENTA CUANTAS VECES ESTÁ UN ELEMENTO EN LA LISTA

# print(versiones_phone.count(4))

#OTRA FORMA DE AGREGAR VALORES A LA LISTA

versiones_phone.extend([8])
versiones_phone.extend(range(3,15))
# print(versiones_phone)

#NOS DICE LA POSICIÓN EN LA QUE ESTÁ UN VALOR DE LA LISTA
# print(versiones_phone.index(5))
#QUE BUSQUE EL 5 DESDE LA POSICION 10
#print(versiones_phone.index(5,10))
'''
try:
    print(versiones_phone.index(5,10))
except:
    print('el numero 5 no se encuentra en la lista')
'''
#AGREGA UN VALOR EN LA POSICION DESEADA, EN ESTE CASO SE PONE UN 1 EN LA POSICION 7
versiones_phone.insert(7,1)
versiones_phone.insert(8,2)
#print(versiones_phone)

'''
#POP ELIMINA EL VALOR QUE ESTÁ EN LA CASILLA QUE SE INDICA
versiones_phone.pop(6)
print(versiones_phone)

#ELIMINA EL VALOR QUE SE INDICA
versiones_phone.remove(3.6)
print(versiones_phone)
'''

#INVIERTE EL ORDEN DE LOS ELEMENTOS DE UNA LISTA
versiones_phone.reverse()
print(versiones_phone)

#ORDENA LOS ELEMENTOS DE UNA LISTA
versiones_phone.sort()
print(versiones_phone)
versiones_phone.sort(reverse=True)
print(versiones_phone)
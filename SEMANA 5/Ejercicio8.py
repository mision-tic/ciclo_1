#import json


#ruta3 = 'c:/Users/Lexsj/MINTIC/CICLO 1/SEMANA 5/archivo.json' 

datos = dict()

datos['clientes'] = []

datos['clientes'].append(
    {
        'first_name': 'Sigrid',
        'last_name': 'Mannock',
        'age':27,
        'amount': 7.17
    }
)

datos['clientes'].append(
    {
        'first_name': 'Joe',
        'last_name': 'Hinners',
        'age':31,
        'amount': [1.90,5.50]
    }
)

datos['clientes'].append(
    {
        'first_name': 'Theodoric',
        'last_name': 'Rivers',
        'age':36,
        'amount': 1.11
    }
)

with open(ruta, 'w') as file:
    json.dump(datos,file, indent=4)
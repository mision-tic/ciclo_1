def tiendaDeProductos(informacion:dict, precio:dict)->dict:    
    def producto(x,y):
        return x*y
    diccionarioProductos = informacion
    diccionarioPrecios = precio
    listaProductos = []
    listaPrecios = []
    longitud = len(informacion["producto01"])
    for valor in diccionarioProductos.values():
        listaProductos.append(valor)
        listaProductos2 = [sum(elemento.values()) for elemento in listaProductos] 

    for valor in diccionarioPrecios.values():
        listaPrecios.append(valor)

    totalHoras = list(map(producto,listaProductos2,listaPrecios))


    totalPorProducto = {"producto0{}".format(i):totalHoras[i-1] for i in range(1,len(totalHoras)+1)}

    productoMayorYMenorRentable = {"productoMayorRentable":[valor for valor in totalPorProducto.keys()
                                                            if totalPorProducto[valor] == max(totalPorProducto.values())],
                                   "productoMenorRentable":[valor for valor in totalPorProducto.keys()
                                                            if totalPorProducto[valor] == min(totalPorProducto.values())]}
    totalDeLaTienda = sum(totalPorProducto.values())

    lista2 = [[listaProductos[i][m] for i in range(len(totalHoras))] for m in range(1,longitud+1)]

    listaPorDia = [list(map(producto,i,listaPrecios)) for i in lista2]
    suma_listaPorDia = [sum(i) for i in listaPorDia]

    totalPorDia = {i:suma_listaPorDia[i-1] for i in range(1,len(suma_listaPorDia)+1)}

    return {"totalPorProducto":totalPorProducto,
            "productoMayorYMenorRentable":productoMayorYMenorRentable,
            "totalDeLaTienda":totalDeLaTienda,
            "totalPorDia":totalPorDia}


print(tiendaDeProductos({"producto01":{1:3,2:5,3:8},
                        "producto02" : {1:5,2:5,3:10},
                        "producto03" : {1:10,2:10,3:9},
                   "producto04":{1:6,2:3,3:7},
                   "producto05":{1:6,2:5,3:9}
                       }, {"producto01" : 12500,
                        "producto02" : 20000,
                        "producto03" : 12000,
                           "producto04":11000,
                           "producto05":23000
                       }))
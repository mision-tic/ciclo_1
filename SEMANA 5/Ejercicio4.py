import pandas as pd

datos = {
    'first_name': ['Sigrid','Joe','Theodoric','Kennedy','Beatrix','Olimpia'],
    'last_name': ['Mannock','Hinners','Rivers','Donnell','Parlett','Guenther'],
    'age': [27, 31, 36, 53, 48, 36],
    'amount_1': [7.17, 1.90, 1.11, 1.41, 6.69, 4.62],
    'amount_2': [8.06, "?", 5.90, "?", "?", 7.48]
}

datosExcel = pd.DataFrame(datos, columns=['first_name','last_name','age','amount_1','amount_2'])

ruta = 'c:/Users/Lexsj/MINTIC/CICLO 1/SEMANA 5/Ejercicio4.xlsx' 

datosExcel.to_excel(ruta,sheet_name='Salario')

def infoIcfes(rt_archivo : str):

    import pandas as pd
    

    if rt_archivo[-3:]  != 'csv':
        return 'Extensión invalida.'
    try:
        #df = pd.read_csv(rt_archivo)
        df = pd.read_csv(rt_archivo, encoding='latin-1', engine='python')
    except:
        return 'Error al leer el archivo de datos.'

    df_filtar = df.query('65 <= punt_matematicas <= 90')
    df_domicilio = df_filtar[['punt_matematicas','departamento_residencia']]

    d_promedio = df_domicilio.groupby("departamento_residencia", as_index=False).mean()
    d_mediana =  df_domicilio.groupby("departamento_residencia", as_index=False).median()
    d_totales =  df_domicilio.groupby("departamento_residencia", as_index=False).count()

    d_datos = pd.merge(d_promedio,d_mediana, on= "departamento_residencia")
    d_datos = pd.merge(d_datos,d_totales, on= "departamento_residencia")
    d_datos.columns = ['Departamento','Promedio','Mediana','Total Puntuación']

    return d_datos.to_dict()
    


print(infoIcfes('https://raw.githubusercontent.com/IsraelArbona/Mision-TIC-GRUPO-09/master/Pruebas_SABER_11_220_estudiantes_2020_1.csv'))

#{'Departamento': {0: 'ATLANTICO', 1: 'BOYACA', 2: 'CALDAS', 3: 'CAQUETA', 4: 'CUNDINAMARCA', 5: 'VALLE'}, 'Promedio': {0: 72.5, 1: 68.0, 2: 75.5, 3: 65.0, 4: 67.0, 5: 72.64}, 'Mediana': {0: 72.0, 1: 68.0, 2: 76.5, 3: 65.0, 4: 67.0, 5: 72.0}, 'Total Puntuación': {0: 42, 1: 2, 2: 4, 3: 1, 4: 2, 5: 25}}

#{'Departamento': {0: 'ATLANTICO', 1: 'BOYACA', 2: 'CALDAS', 3: 'CAQUETA', 4: 'CUNDINAMARCA', 5: 'VALLE'}, 'Promedio': {0: 72.5, 1: 68.0, 2: 75.5, 3: 65.0, 4: 67.0, 5: 72.64}, 'Mediana': {0: 72.0, 1: 68.0, 2: 76.5, 3: 65.0, 4: 67.0, 5: 72.0}, 'Total Puntación': {0: 42, 1: 2, 2: 4, 3: 1, 4: 2, 5: 25}}
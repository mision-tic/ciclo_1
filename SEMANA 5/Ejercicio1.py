#c:/Users/Lexsj/MINTIC/CICLO 1/SEMANA 4/Ejercicio22.py

# libreria pandas

import pandas as pd

datos = {
    'first_name': ['Sigrid','Joe','Theodoric','Kennedy','Beatrix','Olimpia'],
    'last_name': ['Mannock','Hinners','Rivers','Donnell','Parlett','Guenther'],
    'age': [27, 31, 36, 53, 48, 36],
    'amount_1': [7.17, 1.90, 1.11, 1.41, 6.69, 4.62],
    'amount_2': [8.06, "?", 5.90, "?", "?", 7.48]
}

datosDataFrame = pd.DataFrame(datos)
print(datosDataFrame)

#datosDataFrame.to_csv(r'c:\Users\israelarbonaguerrero\Desktop\MISION TIC 2022\CICLO_UNO\GRUPO_39\Semana 5\ejercicio1.csv', sep=';')

datosDataFrame.to_csv('c:/Users/Lexsj/MINTIC/CICLO 1/SEMANA 5/Ejercicio1.csv', sep=';')
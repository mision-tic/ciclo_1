# libreria pandas - dataFrames - archivos csv

import pandas as pd

ruta = 'c:/Users/Lexsj/MINTIC/CICLO 1/SEMANA 5/Ejercicio1.csv'

df = pd.read_csv(ruta, sep=';', skiprows= 1, 
names=['Id','First Name','Last Name','Age','Sales #1','Sales #2'], na_values='?')

# print(df['Sales #2'][2])
# print(df)

df = pd.read_csv(ruta, sep=';', skiprows= 1, 
names=['Id','First Name','Last Name','Age','Sales #1','Sales #2'], na_values='?', index_col='Id')
print(df)

df = pd.read_csv(ruta, sep=';', skiprows= 1, 
names=['Id','First Name','Last Name','Age','Sales #1','Sales #2'], na_values='?', 
index_col=['First Name','Last Name'])
print(df)
# diccionario vacio
dicc = {}

# diccionario campo con valor
diccionario = {'total':55}
#print(diccionario)
otrodiccionario = {'copia':123.23}
#print(otrodiccionario)

'''
diccionario = {'total': 55, 'descuento':True, 15:"15"}
print(diccionario)
diccionarioExcel = {'nombre':5+2,'telefono':3215555555,'edad':38,'ciudad':'pereira'}
print(diccionarioExcel)
diccionario = dict(total = 55, descuento = True, subtotal = 15)
print(diccionario)
diccionario = {'total':55, 10:'curso python',2.0:True}
print(diccionario)
'''

usuario = {
    'nombre': 'Carlos',
    'edad': 22,
    'curso': 'curso de python',
    'skills' :{
        'programacion': True,
        'base_de_datos': True
    },
    'n_medallas':10
}
'''
print(usuario)
print(usuario['curso'])
print(usuario['skills'])
print(usuario['skills']['base_de_datos'])
nuevo_dict = dict()
nuevo_dict['marca'] = 'mazda'
print(nuevo_dict['marca'])
nuevo_dict['marca'] = 'subaru'
print(nuevo_dict['marca'])
print(nuevo_dict)
nuevo_dict = {'Fernando':1,'Carlos':2,'Ramon':3,'Juan':4}
print(nuevo_dict.keys())
print(nuevo_dict.values())
nuevo_dict.clear()
# print(nuevo_dict)
otra_version = nuevo_dict.copy()
print(otra_version)
secuencia = ('python','zope','plone')
version = dict.fromkeys(secuencia)
print(version)
version = dict.fromkeys(secuencia,'1.0')
print(version)
print(version.get('zope'))
print(version.items())
print(version.pop('zope'))
version
print(version)
'''

versiones = dict(python = 3.9, zope = 3.5, plone = 5.1)
print(versiones)
version_adic = dict(django = 4.0)
# version_adic.clear()
print(version_adic)
versiones.update(version_adic)
print(versiones)
print(len(versiones))

del versiones['zope']
print(versiones)
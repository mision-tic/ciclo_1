#Ejercicios semana 2

#1. Leer un número entero y determinar si es un número terminado en 4. 
'''
num = input("Ingrese un numero: ")
num = int(num)
if num%10 == '4':
    print('El número termina en 4')
else:
    print('El número no termina en 4')

'''
#2. Leer un número entero y determinar si tiene 3 dígitos
'''
num = input("Ingrese un numero: ")
num = int(num)
if num > 99 and num < 1000:
    print('El número tiene 3 dígitos')

elif num > -1000 and num < -99:
    print('El número tiene 3 dígitos')

else:
    print('El número no tiene 3 dígitos')    

'''
'''
#3. Leer un número entero de dos dígitos y determinar si ambos dígitos son pares
num = int(input("Ingrese un numero de dos dígitos: "))

if num > 99:
    print("El numero no tiene dos cifras ")
elif  0 < num < 10:
    print("El numero no tiene dos cifras ")
elif  -10 < num < 0 :
    print("El numero no tiene dos cifras ")
elif num <-99:
    print("El numero no tiene dos cifras ")

elif  -99 < num < -10:
    num = num * (-1)
    num_s = str(num)
    d_1 = int(num_s[0])
    d_2 = int(num_s[1])
    if d_1 % 2 == 0 and d_2 % 2==0:
        print("Los dos dígitos del número son pares")
    else:    
        print("Los dos dígitos del número no son pares")
elif 9 < num < 100:
    num_s = str(num)
    d_1 = int(num_s[0])
    d_2 = int(num_s[1])
    if d_1 % 2 == 0 and d_2 % 2==0:
        print("Los dos dígitos del número son pares")
    else:
        print("Los dos dígitos del número no son pares")

'''
'''
#4. Leer un número entero de dos dígitos menor que 20 y determinar si es primo

num = input("Ingrese un número entero de dos cifras menor que 20: ")
num = int(num)
if num > 19:
    print("El número es mayor que 20")

elif 0 < num < 10:
    print("El número no tiene dos cifras")

elif num < 0 :
    print("El número que ingresaste es negativo y un número negativo no puede ser primo")

elif 9 < num < 20:
    if num % 2 == 0 or num % 3 == 0 :
        print("El número NO es primo")
    if num % 3 != 0 and num % 2 != 0 : 
        print("El número SI es primo")    

'''

#5. Leer un número entero de dos dígitos y determinar si es primo y además si es negativo. 

#6. Leer un número entero de dos dígitos y determinar si los dos dígitos son iguales.
'''
num = int(input("Ingrese un numero de dos dígitos: "))

if num > 99:
    print("El numero no tiene dos cifras ")

elif  0 < num < 10:
    print("El numero no tiene dos cifras ") 

elif  -10 < num < 0 :
    print("El numero no tiene dos cifras ")

elif num <-99:
    print("El numero no tiene dos cifras ")

elif  -99 < num < -10:
    num = num * (-1)
    num_s = str(num)
    d_1 = int(num_s[0])
    d_2 = int(num_s[1])
    if d_1 - d_2 == 0 :
        print("Los dos dígitos del número son iguales")
    else:    
        print("Los dos dígitos del número no son iguales")

elif 9 < num < 100:
    num_s = str(num)
    d_1 = int(num_s[0])
    d_2 = int(num_s[1])
    if d_1 - d_2 == 0 :
        print("Los dos dígitos del número son iguales")
    else:
        print("Los dos dígitos del número no son iguales")
'''
#7. Leer dos números enteros y determinar cuál es el mayor. 
'''
num_1 = int(input("Ingresa un entero: "))
num_2 = int(input("Ingresa un entero: "))
if num_1 - num_2 > 0:
    print("EL entero ",num_1 ,"es mayor que ",num_2)
if num_2 - num_1 > 0:
    print("EL entero ",num_2 ,"es mayor que ",num_1)
if num_1 - num_2 == 0:
    print("Los enteros son iguales")
'''
#8. Leer dos números enteros de dos dígitos y determinar a cuánto es igual la suma de todos los dígitos. 
'''
num_1 = int(input("Ingresa un entero: "))
num_2 = int(input("Ingresa un entero: "))
if num_1 < 0:
    num_1 = num_1*(-1)
if num_2 < 0:
    num_2 = num_2*(-1)    


if (9 < num_1 < 100 and 9 < num_2 < 100) or (-99 < num_1 < -10 and -99 < num_2 < -10):
    num_s_1 = str(num_1)
    num_s_2 = str(num_2)
    d_1 = int(num_s_1[0])
    d_2 = int(num_s_1[1])
    d_3 = int(num_s_2[0])
    d_4 = int(num_s_2[1])
    a = d_1 + d_2 + d_3 + d_4
    print(a)
if (-99 < num_1 < -10 and 9 < num_2 < 100) or (9 < num_1 < 100 and -99 < num_2 < -10): 
    num_s_1 = str(num_1)
    num_s_2 = str(num_2)
    d_1 = int(num_s_1[0])
    d_2 = int(num_s_1[1])
    d_3 = int(num_s_2[0])
    d_4 = int(num_s_2[1])
    a = d_1 + d_2 + d_3 + d_4
    print(a) 
'''

#9. Leer un número entero de tres dígitos y determinar en qué posición está el mayor dígito. 
'''
num = int(input("Ingrese un número de tres dígitos: "))
if  -1000 < num < -99:
    num_1 = str(num)
    d_1 = int(num_1[1])
    d_2 = int(num_1[2])
    d_3 = int(num_1[3])
    if d_1 > d_2 > d_3 or d_1 > d_3 > d_2:
        print("El mayor dígito está en la primera posición")
    elif  d_2 > d_1 > d_3 or d_2 > d_3 > d_1: 
        print("El mayor dígito está en la segunda posición")
    elif  d_3 > d_1 > d_2 or d_3 > d_2 > d_1:
        print("El mayor dígito está en la tercera posición")
if 99 < num < 1000:
    num_2 = str(num)
    d_1 = int(num_2[0])
    d_2 = int(num_2[1])
    d_3 = int(num_2[2])
    if d_1 > d_2 > d_3 or d_1 > d_3 > d_2:
        print("El mayor dígito está en la primera posición")
    elif  d_2 > d_1 > d_3 or d_2 > d_3 > d_1: 
        print("El mayor dígito está en la segunda posición")
    elif  d_3 > d_1 > d_2 or d_3 > d_2 > d_1:
        print("El mayor dígito está en la tercera posición")
'''

#10. Leer un número entero de tres dígitos y determinar si algún dígito es múltiplo de los otros
'''
num = int(input("Ingrese un número de tres dígitos: "))
if num < 0:
    num = num*(-1)
num_1 = str(num)
d_1 = int(num_1[0])
d_2 = int(num_1[1])
d_3 = int(num_1[2])
if d_1 % d_2 == 0:
     print(d_1, " es múltiplo de ", d_2)
if d_1 % d_3 == 0:
     print(d_1, " es múltiplo de ", d_3)
if d_2 % d_1 == 0:
     print(d_2, " es múltiplo de ", d_1)     
if d_2 % d_3 == 0:
     print(d_2, " es múltiplo de ", d_3) 
if d_3 % d_1 == 0:
     print(d_3, " es múltiplo de ", d_1) 
if d_3 % d_2 == 0:
     print(d_3, " es múltiplo de ", d_2)    

'''

#11. Leer tres números enteros de dos dígitos cada uno y determinar en cuál de ellos se encuentra
#el mayor dígito.
'''
num_1 = int(input("Ingrese un número de dos dígitos: "))
num_2 = int(input("Ingrese un número de dos dígitos: "))
num_3 = int(input("Ingrese un número de dos dígitos: "))

if num_1 < 0:
    mun_1 = num_1*(-1)
if num_2 < 0:
    mun_2 = num_2*(-1)
if num_3 < 0:
    mun_3 = num_3*(-1)
if 9 < num_1 < 100 and 9 < num_2 < 100 and 9 < num_3 < 100:
    num_s_1 = str(num_1)
    num_s_2 = str(num_2)
    num_s_3 = str(num_3)
    d_1 = int(num_s_1[0])
    d_2 = int(num_s_1[1])
    d_3 = int(num_s_2[0])
    d_4 = int(num_s_2[1])
    d_5 = int(num_s_3[0])
    d_6 = int(num_s_3[1])
    if d_1 > d_2 and d_1 > d_3 and d_1 > d_4 and d_1 > d_5 and d_1 > d_6:
        print("El mayor dígito es ", d_1," y está en el número ",num_1)
    if d_2 > d_1 and d_2 > d_3 and d_2 > d_4 and d_2 > d_5 and d_2 > d_6:
        print("El mayor dígito es ", d_2," y está en el número ",num_1)
    if d_3 > d_2 and d_3 > d_1 and d_3 > d_4 and d_3 > d_5 and d_3 > d_6:
        print("El mayor dígito es ", d_3," y está en el número ",num_2)
    if d_4 > d_2 and d_4 > d_3 and d_4 > d_1 and d_4 > d_5 and d_4 > d_6:
        print("El mayor dígito es ", d_4," y está en el número ",num_2)
    if d_5 > d_2 and d_5 > d_3 and d_5 > d_4 and d_5 > d_1 and d_5 > d_6:
        print("El mayor dígito es ", d_5," y está en el número ",num_3)
    if d_6 > d_2 and d_6 > d_3 and d_6 > d_4 and d_6 > d_5 and d_6 > d_1:
        print("El mayor dígito es ", d_6," y está en el número ",num_3)

'''


#12. Leer un número entero de suma de los otros dos. 



#13. Leer un número entero menor que 50 y positivo y determinar si es un número primo.

#14. Leer un número entero de cuatro dígitos y determinar si el segundo dígito es igual al
#penúltimo.
'''
num = int(input("Ingrese un número: "))
if num < 0:
    num = num*(-1)
if 999 < num < 10000:
    num_s = str(num)
    d_1 = int(num_s[0])
    d_2 = int(num_s[1])
    d_3 = int(num_s[2])
    d_4 = int(num_s[3])
    if d_2 == d_3:
        print("El segundo dígito y el penultimo son iguales")
    else:
        print("El segundo dígito y el penultimo no son iguales")
else:
    print("Vuelve a intentar con un número de cuatro dígitos")
'''

#15. Leer un número entero y determinar si es múltiplo de 7. 
'''
num = int(input("Ingrese un número: "))
if num % 7 == 0:
    print("El número ",num," es múltiplo de 7")
else:
    print("El número ",num," no es múltiplo de 7")
'''    
#16. Leer un número entero menor que mil y determinar cuántos dígitos tiene.
'''
num = int(input("Ingrese un número menor a 1000: "))
num_s = str(num)
Lon = len(num_s)
if num < 0:
    d = Lon -1
    print("El número",num, "tiene", d, "dígitos")
elif  0 < num < 1000:
    print("El número",num,"tiene",Lon,"digitos")
elif num == 0:
    print("El número", num, "tiene 1 dígito")   
elif num >= 1000:
    print("Prueba con un número menor a 1000")     
'''
#17. Leer un número entero de 4 dígitos y determinar si tiene mas dígitos pares o impares.
'''
#NO SE COMO HACERLO
num = int(input("Ingrese un número: "))
if num < 0:
    num = num*(-1)
if 999 < num < 10000:
    num_s = str(num)
    d_1 = int(num_s[0])
    d_2 = int(num_s[1])
    d_3 = int(num_s[2])
    d_4 = int(num_s[3])
    if d_1 % 2 == 0 and d_2 % 2 == 0 and d_3 % 2 == 0 and d_4 % 2 == 0:
        print("Tiene cuatro dígitos pares")
    if d_1 % 2 == 0 and d_1 % 2 == 0 and d_1 % 2 == 0 and d_1 % 2 != 0:

    if d_1 % 2 == 0 and d_1 % 2 == 0 and d_1 % 2 == 0 and d_1 % 2 == 0:

    if d_1 % 2 == 0 and d_1 % 2 == 0 and d_1 % 2 == 0 and d_1 % 2 == 0:            
'''
#18. Leer tres números enteros y determinar si el último dígito de los tres números es igual. 
'''
num_1 = int(input("Ingrese un número: "))
num_2 = int(input("Ingrese un número: "))
num_3 = int(input("Ingrese un número: "))

if num_1 < 0:
    mun_1 = num_1*(-1)
if num_2 < 0:
    mun_2 = num_2*(-1)
if num_3 < 0:
    mun_3 = num_3*(-1)

num_s_1 = str(num_1)
num_s_2 = str(num_2)
num_s_3 = str(num_3)

L_1 = len(num_s_1)-1
L_2 = len(num_s_2)-1
L_3 = len(num_s_3)-1

d_1 = int(num_s_1[L_1]) 
d_2 = int(num_s_2[L_2])
d_3 = int(num_s_3[L_3])  

if d_1 == d_2 and d_2 == d_3:
    print("El último dígito de los números",num_1,num_2,"y",num_3,"son iguales")
else:
    print("El último dígito de los números",num_1,num_2,"y",num_3,"no son iguales")
'''

#19. A un trabajador le pagan según sus horas trabajadas por una tarifa de pago por hora. Si la
#cantidad de horas trabajadas es mayor 40 horas. La tarifa se incrementa en un 50% para las
#horas extras. Calcular el salario del trabajador dadas las horas trabajadas y la tarifa
#ingresadas por el usuario.

#20. Hacer un algoritmo que calcule el total a pagar por la compra de camisas. Si se compran
#tres camisas o más se aplica un descuento del 20% sobre el total de la compra y si son
#menos de tres camisas un descuento del 10%.
'''
num_camisas = int(input("Cuántas camisas vas a comprar?:"))
precio = int(input("Ingrese el precio de las camisas: "))
if num_camisas >= 3:
    total = (num_camisas*precio)-(num_camisas*precio)*(0.20)
    print("Debes pagar en total",total)
elif num_camisas < 3:
    total = (num_camisas*precio)-(num_camisas*precio)*(0.10)
    print("Debes pagar en total",total)

'''    
#21. Hacer un programa que pida al usuario las tres notas de un alumno (deben estar entre 0 y
#5 y pueden tener decimales) y calcule el promedio e indique mediante un mensaje si aprobó
#o no (aprueba con nota mayor a 3). Se debe validar que las notas introducidas estén dentro
#del rango permitido.
'''
nota_1 = float(input("Ingresa la primera nota del estudiante: "))
nota_2 = float(input("Ingresa la segunda nota del estudiante: "))
nota_3 = float(input("Ingresa la tercera nota del estudiante: "))

if 0 < nota_1 < 5 and 0 < nota_2 < 5 and 0 < nota_3 < 5:
    promedio = (nota_1 + nota_2 + nota_3)/3
    if promedio > 3:
        print("El estudiante aprobó la materia con una nota de ", promedio)
    elif promedio <= 3: 
        print("El estudiante perdió la materia con una nota de ", promedio)   
else:
    print("Revisa el rango de las notas y vuelve a intentar")
'''
#22. Verificar si un texto que termina en punto es un palíndromo (capicúa). Un texto es
#palíndromo si se lee lo mismo de izquierda a derecha o de derecha a izquierda. Ej: “Amor
#a roma”.


#23. Diseñe una calculadora que sume, reste, multiplique y divida, la cual le pida al usuario
#mediante input el valor del primer número, el valor del segundo número y la operación a
#realizar, hay que tener en cuenta la validación de los valores de entrada
'''
num_1 = float(input("Ingrese el primer número: "))
num_2 = float(input("Ingrese el segundo número: "))

operación = input("Que operación deseas realizar? \n\n Marca [1] para suma. \n\n Marca [2] para resta
 \n\n Marca [3] para multiplicación \n \n Marca [4] para división: \n")

if operación == '1':
    suma = num_1 + num_2
    print("La suma de los valores es: ", suma)
elif operación == '2':
    resta = num_1 - num_2
    print("La resta de los valores es: ", resta)

elif operación == '3':
    multiplicación = num_1*num_2
    print("La multiplicación de los valores es: ", multiplicación)

elif operación == '4':     
    división = num_1 / num_2
    print("La división de los valores es: ", división)       
'''    
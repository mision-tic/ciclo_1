
def Diagnosticar_Sintoma(Paciente: dict)->dict:
    if Paciente["diag_ta"]=='No':
        if Paciente["diag_pa"]=='No':
            if Paciente['diag_do']=='No' or Paciente['diag_dg']=='No' or Paciente['diag_ca']=='No':
                sintomas = 'No tiene síntomas'
            else:
                sintomas = 'Presencia de síntomas'
        else:
            if Paciente['diag_do']=='No':
                sintomas = 'Presencia de síntomas'
            elif Paciente['diag_dg']=='No':
                sintomas = 'Presencia de síntomas'
            elif Paciente['diag_ca']=='No':
                sintomas = 'Otitis'
            else:  
                sintomas = 'Presencia de síntomas'                     
    else:
        if Paciente["diag_pa"]=='No':
            if Paciente['diag_do']=='No':
                if Paciente['diag_dg']=='No': 
                    sintomas = 'Presencia de síntomas'
                else:
                    if Paciente['diag_ca']=='No':
                        sintomas = 'Presencia de síntomas'
                    else:
                        sintomas = 'Gripa'
        if Paciente["diag_pa"]=='Si':
            if Paciente['diag_do']=='No':
                sintomas = 'Presencia de síntomas'
            else:
                if Paciente['diag_dg']=='No':
                    sintomas = 'Presencia de síntomas'
                else:
                    if Paciente['diag_ca']=='No':
                        sintomas = 'Presencia de síntomas'
                    else:
                        sintomas = 'Covid 19'
    if sintomas == 'Covid 19' or  sintomas == 'Otitis' or  sintomas == 'Gripa':
        estado_p = True
    else:
         estado_p = False 
         
    dic = {'id_diagnostico': Paciente['id_diagnostico'], 'resultado': sintomas, 'estado': estado_p}                    
    return dic


print(Diagnosticar_Sintoma({'id_diagnostico':'d-003', "diag_ta": 'No', "diag_pa": 'No', "diag_do": 'No', "diag_dg": 'No', "diag_ca": 'No'}))
print(Diagnosticar_Sintoma({'id_diagnostico':'d-001','diag_ta':'Si','diag_pa':'No','diag_do':'No','diag_dg':'Si','diag_ca':'Si'}))
print(Diagnosticar_Sintoma({'id_diagnostico':'d-002','diag_ta':'Si','diag_pa':'No','diag_do':'No','diag_dg':'No','diag_ca':'No'}))
print(Diagnosticar_Sintoma({'id_diagnostico':'d-002','diag_ta':'Si','diag_pa':'No','diag_do':'No','diag_dg':'No','diag_ca':'No'}))
#dic = {'id_diagnostico': , 'resultado':, 'estado':}
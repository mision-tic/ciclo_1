def testNumero():
    entero = int(input("ingresar el numero :"))
    if entero == 0:
        return "el numero no puede ser cero"
    if entero > 0:
        if len(str(entero)) == 2:
            return "el numero es positivo y tiene dos digitos"
        else:
            return "el numero es positivo y tiene mas o menos de dos digitos"
    if entero < 0:
        # entero = entero *(-1)
        # if len(str(entero)) == 4:
        if len(str(entero)) - 1 == 3:
            return "el numero es negativo y tiene tres digitos"
        else:
            return "el numero es negativo y tiene mas o menos de tres digitos"